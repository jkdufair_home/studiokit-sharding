﻿using Microsoft.Azure;
using StudioKit.Configuration;
using System;
using System.Collections.Generic;

namespace StudioKit.Sharding
{
	public class ShardDomainConfiguration
	{
		public ShardDomainConfiguration()
		{
			RootDomainParts = new List<string> { "www", "dev" };
		}

		public ShardDomainConfiguration(string hostname) : this()
		{
			if (string.IsNullOrWhiteSpace(hostname))
				throw new ArgumentNullException(nameof(hostname));
			Hostname = hostname;
			RootDomainParts.Add(hostname.Split('.')[0]);
		}

		public string Hostname { get; }

		public List<string> RootDomainParts { get; set; }

		public string BaseUrlWithShardKey(string shardKey)
		{
			var tier = CloudConfigurationManager.GetSetting(BaseAppSetting.Tier, false);
			return $"https://{shardKey}{(tier == Tier.Dev ? ".dev" : "")}.{Hostname}/";
		}

		#region Singleton

		private static ShardDomainConfiguration _instance;

		public static ShardDomainConfiguration Instance
		{
			get => _instance ?? (_instance = new ShardDomainConfiguration());
			set => _instance = value;
		}

		#endregion Singleton
	}
}