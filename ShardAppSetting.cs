﻿namespace StudioKit.Sharding
{
	public static class ShardAppSetting
	{
		public const string ApplicationName = "ApplicationName";

		public const string ShardMapManagerDatabaseName = "ShardMapManagerDatabaseName";

		public const string ShardMapManagerDatabaseEdition = "ShardMapManagerDatabaseEdition";

		public const string ShardDatabaseEdition = "ShardDatabaseEdition";

		public const string SqlUserName = "SqlUserName";

		public const string SqlPassword = "SqlPassword";

		public const string SqlServerName = "SqlServerName";

		public const string SqlMaxPoolSize = "SqlMaxPoolSize";

		public const string SqlConnectTimeout = "SqlConnectTimeout";

		public const string LocalhostShardKey = "LocalhostShardKey";
	}
}