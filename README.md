﻿# StudioKit.Sharding

**Note**: Requires the following peer dependencies in the Solution.
* **StudioKit.Caliper.Interfaces**
* **StudioKit.Data**
* **StudioKit.Encryption**
* **StudioKit.Utilities**

Classes for managing Shard Databases.

## Implementation

1. In your `Startup.cs`, define the `ShardDomainConfiguration` with your app's hostname.

```
ShardDomainConfiguration.Instance = new ShardDomainConfiguration("studypattern.org");
```

2. Using a subclass of `ShardManager`, initialize your shard settings.

```
// Instantiate the ShardManager, Shard Map Manager and ShardMap at StartUp
var patternShardManager = new PatternShardManager();
ShardManager.Instance = patternShardManager;
patternShardManager.CreateOrGetShardMapDbAndManager();
patternShardManager.CreateOrGetDefaultShardMap();
```

## Console App

Create a console application project in order to use the shard manager commands. You must have all required settings in your App.config.

```
using System;
using Pattern.Utility;

namespace Pattern.ShardManager
{
	public class Program
	{
		[STAThread] // enable open file dialog
		public static void Main()
		{
			new PatternShardManager().StartConsoleApp();
		}
	}
}
```
