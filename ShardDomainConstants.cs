﻿namespace StudioKit.Sharding
{
	public class ShardDomainConstants
	{
		public static string InvalidShardKey => "invalid-shard";

		public static string RootShardKey => "root-shard";

		public static string PurdueShardKey => "purdue";

		public static string DemoShardKey => "demo";

		public static string Localhost => "localhost";

		public static string AzureCloudServiceHostname => "cloudapp.net";

		public static string AzureWebAppHostname => "azurewebsites.net";
	}
}