﻿using StudioKit.Data;
using System;
using StudioKit.Sharding.Interfaces;

namespace StudioKit.Sharding.Models
{
	public class BaseLicense : ModelBase, IBaseLicense
	{
		public DateTime StartDate { get; set; }

		public DateTime EndDate { get; set; }
	}
}