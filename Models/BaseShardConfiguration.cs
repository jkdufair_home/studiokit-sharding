using StudioKit.Data;
using StudioKit.Sharding.Interfaces;
using StudioKit.Utilities.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.Sharding.Models
{
	public class BaseShardConfiguration : ModelBase, IBaseShardConfiguration
	{
		public string Name { get; set; }

		public bool TrialEnabled { get; set; }

		public int? TrialDefaultDayLimit { get; set; }

		public string UserSupportEmail { get; set; }

		public string AllowedDomains { get; set; }

		public byte[] Image { get; set; }

		public string DefaultTimeZoneId { get; set; }

		public bool CaliperEnabled { get; set; }

		public string CaliperEventStoreClientId { get; set; }

		public string CaliperEventStoreClientSecret { get; set; }

		public string CaliperEventStoreHostname { get; set; }

		public string CaliperPersonNamespace { get; set; }

		public string RedisConnectionString { get; set; }

		public bool RedisShouldCollectStatistics { get; set; }

		public bool IsInstructorSandboxEnabled { get; set; }

		public int? DemoExpirationDayLimit { get; set; }

		public bool ValidateAllowedDomains(string scopedIdentifier)
		{
			if (string.IsNullOrWhiteSpace(AllowedDomains))
				return true;

			if (!scopedIdentifier.HasValidDomainScope())
				return false;

			var domains = new List<string>(AllowedDomains.Split(','));
			var userDomain = scopedIdentifier.GetDomain();

			// checks if user email domain contains any allowed domains
			// e.g. "person@purdue.edu" contains "purdue.edu"
			// e.g. "person@ecn.purdue.edu" contains "purdue.edu"
			return domains.Any(emailDomain => userDomain.ToLowerInvariant().Contains(emailDomain.ToLowerInvariant()));
		}
	}
}