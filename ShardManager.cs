﻿using Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement;
using StudioKit.Sharding.Extensions;
using StudioKit.Sharding.Utils;
using System;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace StudioKit.Sharding
{
	public class ShardManager
	{
		public ShardMapManager ShardMapManger { get; private set; }

		public ListShardMap<byte[]> DefaultShardMap { get; private set; }

		protected string CustomerName;
		protected string ShardKey;
		protected bool IsConsole;

		protected ShardManager()
		{
			IsConsole = false;
		}

		#region Singleton

		private static ShardManager _instance;

		public static ShardManager Instance
		{
			get => _instance ?? (_instance = new ShardManager());
			set => _instance = value;
		}

		#endregion Singleton

		public string Subdomain => ShardConfiguration.Tier == Configuration.Tier.Dev ? $"{ShardKey}.dev" : ShardKey;

		/// <summary>
		/// Creates a shard map manager, its database, and a shard map.
		/// </summary>
		public ShardMapManager CreateOrGetShardMapDbAndManager()
		{
			if (ShardMapManger != null)
			{
				ConsoleUtils.WriteWarning("Shard Map Manager already exists");
				return ShardMapManger;
			}

			// Create shard map manager database
			if (!SqlDatabaseUtils.DatabaseExists(
				ShardConfiguration.ShardMapManagerServerName,
				ShardConfiguration.ShardMapManagerDatabaseName))
			{
				SqlDatabaseUtils.CreateDatabase(
					ShardConfiguration.ShardMapManagerServerName,
					ShardConfiguration.ShardMapManagerDatabaseName,
					ShardConfiguration.ShardMapManagerDatabaseEdition);
			}

			// Create shard map manager
			var shardMapManagerConnectionString =
				ShardConfiguration.GetConnectionString(
					ShardConfiguration.ShardMapManagerServerName,
					ShardConfiguration.ShardMapManagerDatabaseName);

			ShardMapManger = ShardUtils.CreateOrGetShardMapManager(shardMapManagerConnectionString);

			return ShardMapManger;
		}

		public ListShardMap<byte[]> CreateOrGetDefaultShardMap()
		{
			if (DefaultShardMap != null)
			{
				ConsoleUtils.WriteWarning("Default Shard Map already exists");
				return DefaultShardMap;
			}

			// Create shard map
			DefaultShardMap = ShardUtils.CreateOrGetListShardMap<byte[]>(
				ShardMapManger,
				ShardConfiguration.ShardMapName);
			return DefaultShardMap;
		}

		public void AddShard(string customerName, string databaseName, string shardKey)
		{
			var shardMap = TryGetShardMap();
			if (shardMap == null)
				return;

			var shardMapping = TryGetShardMappingByShardKey(shardKey);
			if (shardMapping != null)
			{
				ConsoleUtils.WriteWarning("Shard Mapping already exists");
				return;
			}

			// store name and key on protected variables
			CustomerName = customerName;
			ShardKey = shardKey;

			// Create a new shard, or get an existing empty shard (if a previous create partially succeeded).
			var shard = ShardUtils.CreateOrGetEmptyShard(
				shardMap,
				databaseName,
				ShardConfiguration.ShardServerName,
				ShardConfiguration.ShardDatabaseEdition);

			// Create a mapping to that shard.
			var mappingForNewShard = shardMap.CreatePointMapping(shardKey.ToPointMapping(), shard);
			ConsoleUtils.WriteInfo("Mapped point \"{0}\" to shard \"{1}\"",
				Encoding.UTF8.GetString(mappingForNewShard.Value), shard.Location.Database);

			// Run any migration scripts or methods on the shard database.
			MigrateShardDatabase(shard.Location);

			// Run any seed scripts or methods on the shard database.
			SeedShardDatabase(shard.Location);

			// clear out creation variables
			CustomerName = null;
			ShardKey = null;
		}

		public void MigrateAllShardDatabases()
		{
			var shardMap = TryGetShardMap();
			if (shardMap == null) return;

			foreach (var shard in shardMap.GetShards())
			{
				MigrateShardDatabase(shard.Location);
			}
		}

		public void SeedAllShardDatabases()
		{
			var shardMap = TryGetShardMap();
			if (shardMap == null) return;

			foreach (var shard in shardMap.GetShards())
			{
				ConsoleUtils.WriteColor(ConsoleColor.Green, "You are editing Configuration for \"{0}\" - \"{1}\"",
					shard.Location.Server, shard.Location.Database);
				SeedShardDatabase(shard.Location);
			}
		}

		/// <summary>
		/// Drop a specific shard database.
		/// </summary>
		/// <param name="shardMapping"></param>
		public void DropShardDatabase(PointMapping<byte[]> shardMapping)
		{
			var shard = shardMapping.Shard;
			var location = shard.Location;

			ConsoleUtils.WriteColor(ConsoleColor.Green, "Dropping Shard \"{0}\" - \"{1}\"",
				location.Server, location.Database);

			var shardMap = TryGetShardMap();
			if (shardMap == null) return;
			shardMapping = shardMap.MarkMappingOffline(shardMapping);
			shardMap.DeleteMapping(shardMapping);

			shard = ShardUtils.CreateOrGetShard(shardMap, location);
			shardMap.DeleteShard(shard);
			SqlDatabaseUtils.DropDatabase(location.DataSource, location.Database);
		}

		/// <summary>
		/// Drops all shards and the shard map manager database (if it exists).
		/// </summary>
		private void DropAll()
		{
			var shardMap = TryGetShardMap();
			if (shardMap != null)
			{
				foreach (var shard in shardMap.GetShards())
				{
					ConsoleUtils.WriteColor(ConsoleColor.Green, "Dropping Shard \"{0}\" - \"{1}\"",
						shard.Location.Server, shard.Location.Database);
					SqlDatabaseUtils.DropDatabase(shard.Location.DataSource, shard.Location.Database);
				}
			}

			if (SqlDatabaseUtils.DatabaseExists(ShardConfiguration.ShardMapManagerServerName, ShardConfiguration.ShardMapManagerDatabaseName))
			{
				// Drop shard map manager database
				SqlDatabaseUtils.DropDatabase(ShardConfiguration.ShardMapManagerServerName, ShardConfiguration.ShardMapManagerDatabaseName);
			}

			// Since we just dropped the shard map manager database, this shardMapManager reference is now non-functional.
			// So set it to null so that the program knows that the shard map manager is gone.
			ShardMapManger = null;
		}

		/// <summary>
		/// Override point for subclasses to run database migrations on a shard database.
		/// </summary>
		/// <param name="location">The location of the shard database. </param>
		public virtual void MigrateShardDatabase(ShardLocation location) { }

		/// <summary>
		/// Override point for subclasses to run database seed methods on a shard database.
		/// </summary>
		/// <param name="location">The location of the shard database. </param>
		public virtual void SeedShardDatabase(ShardLocation location) { }

		#region Console App

		private const ConsoleColor EnabledColor = ConsoleColor.White;

		private const ConsoleColor DisabledColor = ConsoleColor.DarkGray;

		private enum MenuChoice
		{
			CreateShardMapManagerAndDatabase = 1,
			AddShard,
			MigrateShard,
			SeedShard,
			DropShard,
			MigrateAll,
			SeedAll,
			DropAll,
			ChangeTier,
			Exit
		}

		private string MenuChoiceText(MenuChoice menuChoice)
		{
			switch (menuChoice)
			{
				case MenuChoice.CreateShardMapManagerAndDatabase:
					return "Create shard map manager and database";

				case MenuChoice.AddShard:
					return "Add a new shard";

				case MenuChoice.MigrateShard:
					return "Run DB Migration on a shard";

				case MenuChoice.SeedShard:
					return "Add or Update Configuration on a shard";

				case MenuChoice.DropShard:
					return "Drop a shard";

				case MenuChoice.MigrateAll:
					return "Run DB Migration on all shards";

				case MenuChoice.SeedAll:
					return "Add or Update Configuration on all shards";

				case MenuChoice.DropAll:
					return "Drop all shards";

				case MenuChoice.ChangeTier:
					return "Change Tier";

				case MenuChoice.Exit:
					return "Exit";

				default:
					throw new ArgumentOutOfRangeException(nameof(menuChoice), menuChoice, null);
			}
		}

		public void StartConsoleApp()
		{
			IsConsole = true;

			// Welcome screen
			Console.WriteLine("***********************************************************");
			Console.WriteLine("*******    Welcome to the {0} Shard Manager    ******", ShardConfiguration.ApplicationName);
			Console.WriteLine("***********************************************************");
			Console.WriteLine();

			// Prompt for Tier
			if (!SetAndConnectToTier()) return;

			// Connection succeeded. Begin interactive loop
			MenuLoop();
		}

		private static void PrintServer()
		{
			ConsoleUtils.WriteColor(ConsoleColor.Green, "You are connected to the {0} database: \"{1}\"",
				ShardConfiguration.Tier, ShardConfiguration.ShardMapManagerServerName);
		}

		private static bool ConfirmServerExists()
		{
			// Verify that we can connect to the Sql Database that is specified in App.config settings
			if (SqlDatabaseUtils.TryConnectToSqlServer(ShardConfiguration.ShardMapManagerServerName))
			{
				return true;
			}

			// Connecting to the server failed - please update the settings in App.Config

			if (!Debugger.IsAttached) return false;

			// Give the user a chance to read the message, if this program is being run
			// in debug mode in Visual Studio
			Console.WriteLine("Press ENTER to continue...");
			Console.ReadLine();

			// Exit
			return false;
		}

		/// <summary>
		/// Main program loop.
		/// </summary>
		private void MenuLoop()
		{
			// Loop until the user chose "Exit".
			bool continueLoop;
			do
			{
				PrintShardMapState();
				Console.WriteLine();

				PrintMenu();
				Console.WriteLine();

				continueLoop = GetMenuChoiceAndExecute();
				Console.WriteLine();
			} while (continueLoop);
		}

		/// <summary>
		/// Writes the shard map's state to the console.
		/// </summary>
		private void PrintShardMapState()
		{
			PrintServer();

			Console.WriteLine("Current Shard Map state:");
			var shardMap = TryGetShardMap();
			if (shardMap == null)
			{
				return;
			}

			// Get all shards
			var allShards = shardMap.GetShards();

			// Get all mappings, grouped by the shard that they are on. We do this all in one go to minimize round trips.
			var mappingsGroupedByShard = shardMap.GetMappings().ToLookup(m => m.Shard);

			if (allShards.Any())
			{
				// The shard map contains some shards, so for each shard (sorted by database name)
				// write out the mappings for that shard
				foreach (var shard in shardMap.GetShards().OrderBy(s => s.Location.Database))
				{
					var mappingsOnThisShard = mappingsGroupedByShard[shard];

					// avoid multiple enumeration
					var mappingsArray = mappingsOnThisShard as PointMapping<byte[]>[] ?? mappingsOnThisShard.ToArray();

					if (mappingsArray.Any())
					{
						var mappingsString = string.Join(", ", mappingsArray.Select(m => Encoding.UTF8.GetString(m.Value)));
						Console.WriteLine("\t\"{0}\" contains point mappings \"{1}\"", shard.Location.Database, mappingsString);
					}
					else
					{
						Console.WriteLine("\t\"{0}\" contains no mappings.", shard.Location.Database);
					}
				}
			}
			else
			{
				Console.WriteLine("\tShard Map contains no shards");
			}
		}

		/// <summary>
		/// Writes the program menu.
		/// </summary>
		private void PrintMenu()
		{
			ConsoleColor createSmmColor; // color for create shard map manger menu item
			ConsoleColor otherMenuItemColor; // color for other menu items
			if (ShardMapManger == null)
			{
				createSmmColor = EnabledColor;
				otherMenuItemColor = DisabledColor;
			}
			else
			{
				createSmmColor = DisabledColor;
				otherMenuItemColor = EnabledColor;
			}

			foreach (MenuChoice menuChoice in Enum.GetValues(typeof(MenuChoice)))
			{
				ConsoleUtils.WriteColor(menuChoice == MenuChoice.CreateShardMapManagerAndDatabase
					? createSmmColor
					: menuChoice == MenuChoice.Exit
						? EnabledColor
						: otherMenuItemColor, $"{(int)menuChoice}. {MenuChoiceText(menuChoice)}");
			}
		}

		/// <summary>
		/// Gets the user's chosen menu item and executes it.
		/// </summary>
		/// <returns>true if the program should continue executing.</returns>
		private bool GetMenuChoiceAndExecute()
		{
			while (true)
			{
				var inputValue = (MenuChoice)ConsoleUtils.ReadIntegerInput($"Enter an option [1-{(int)MenuChoice.Exit}] and press ENTER: ");

				switch (inputValue)
				{
					case MenuChoice.CreateShardMapManagerAndDatabase:
						Console.WriteLine();
						CreateOrGetShardMapDbAndManager();
						CreateOrGetDefaultShardMap();
						return true;

					case MenuChoice.AddShard:
						Console.WriteLine();
						ReadCustomerNameAndAddShard();
						return true;

					case MenuChoice.MigrateShard:
						Console.WriteLine();
						ChooseAndMigrateShard();
						return true;

					case MenuChoice.SeedShard:
						Console.WriteLine();
						ChooseAndSeedShard();
						return true;

					case MenuChoice.DropShard:
						Console.WriteLine();
						ChooseAndDropShard();
						return true;

					case MenuChoice.MigrateAll:
						Console.WriteLine();
						MigrateAllShardDatabases();
						return true;

					case MenuChoice.SeedAll:
						Console.WriteLine();
						SeedAllShardDatabases();
						return true;

					case MenuChoice.DropAll:
						Console.WriteLine();
						ConfirmDropAll();
						return true;

					case MenuChoice.ChangeTier:
						Console.WriteLine();
						return SetAndConnectToTier();

					case MenuChoice.Exit:
						return false;
				}
			}
		}

		private static void SetTier()
		{
			while (true)
			{
				Console.Write("Enter a tier (LOCAL, DEV, or PROD) and press ENTER: ");
				var tier = Console.ReadLine();
				if (string.IsNullOrWhiteSpace(tier) || tier != Configuration.Tier.Local &&
					tier != Configuration.Tier.Dev && tier != Configuration.Tier.Prod)
				{
					ConsoleUtils.WriteWarning("Invalid tier, try again");
					continue;
				}

				ShardConfiguration.Tier = tier;
				ConsoleUtils.WriteColor(ConsoleColor.Green, "Tier set to \"{0}\"", tier);
				break;
			}
		}

		private bool SetAndConnectToTier()
		{
			SetTier();

			SetCredentials();

			if (!ConfirmServerExists()) return false;

			// replace ShardMapManager after change
			ShardMapManger = ShardUtils.TryGetShardMapManager(
				ShardConfiguration.ShardMapManagerServerName,
				ShardConfiguration.ShardMapManagerDatabaseName);

			return true;
		}

		private static void SetCredentials()
		{
			ShardConfiguration.UserId = string.Empty;
			ShardConfiguration.Password = string.Empty;

			if (ShardConfiguration.Tier == Configuration.Tier.Local) return;

			string userId;
			while (true)
			{
				Console.Write("Enter your DB UserId and press ENTER: ");
				userId = Console.ReadLine();
				if (string.IsNullOrWhiteSpace(userId))
				{
					ConsoleUtils.WriteWarning("UserId required, try again");
					continue;
				}

				break;
			}

			var password = "";
			while (true)
			{
				Console.Write("Enter your DB Password and press ENTER: ");
				do
				{
					var key = Console.ReadKey(true);
					// Backspace Should Not Work
					if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
					{
						password += key.KeyChar;
						Console.Write("*");
					}
					else
					{
						if (key.Key == ConsoleKey.Backspace && !string.IsNullOrWhiteSpace(password))
						{
							password = password.Substring(0, (password.Length - 1));
							Console.Write("\b \b");
						}
						else if (key.Key == ConsoleKey.Enter)
						{
							break;
						}
					}
				} while (true);

				if (string.IsNullOrWhiteSpace(password))
				{
					ConsoleUtils.WriteWarning("Password required, try again");
					continue;
				}

				break;
			}

			ShardConfiguration.UserId = userId;
			ShardConfiguration.Password = password;

			Console.WriteLine();
			ConsoleUtils.WriteColor(ConsoleColor.Green, "UserId and Password set");
		}

		/// <summary>
		/// Reads the user's choice of a customer name, and creates a new shard with a mapping for the resulting customer.
		/// </summary>
		private void ReadCustomerNameAndAddShard()
		{
			Console.Write("Enter a new customer name and press ENTER: ");
			var customerName = Console.ReadLine();
			if (string.IsNullOrWhiteSpace(customerName))
				return;

			Console.Write("Enter a new database name and press ENTER: ");
			var databaseName = Console.ReadLine().ToShardDatabaseName(ShardConfiguration.ShardDatabaseNameFormat); ;
			if (string.IsNullOrWhiteSpace(databaseName))
				return;

			Console.Write("Enter a new customer key and press ENTER: ");
			var customerKey = Console.ReadLine().ToShardKey();
			if (string.IsNullOrWhiteSpace(customerKey))
				return;

			Console.WriteLine();
			Console.WriteLine("Creating shard \"{0}\" for customer \"{1}\" with key \"{2}\"", databaseName, customerName, customerKey);

			AddShard(customerName, databaseName, customerKey);
		}

		private void ChooseAndMigrateShard()
		{
			var customerKey = ReadShardCustomerKey("migrate");
			if (string.IsNullOrWhiteSpace(customerKey))
				return;

			var shardMapping = TryGetShardMappingByShardKey(customerKey);
			if (shardMapping == null)
			{
				ConsoleUtils.WriteWarning("Shard Mapping not found");
				return;
			}

			MigrateShardDatabase(shardMapping.Shard.Location);
		}

		private void ChooseAndSeedShard()
		{
			var customerKey = ReadShardCustomerKey("seed");
			if (string.IsNullOrWhiteSpace(customerKey))
				return;

			var shardMapping = TryGetShardMappingByShardKey(customerKey);
			if (shardMapping == null)
			{
				ConsoleUtils.WriteWarning("Shard Mapping not found");
				return;
			}

			SeedShardDatabase(shardMapping.Shard.Location);
		}

		private void ChooseAndDropShard()
		{
			var customerKey = ReadShardCustomerKey("drop");
			if (string.IsNullOrWhiteSpace(customerKey))
				return;

			var shardMapping = TryGetShardMappingByShardKey(customerKey);
			if (shardMapping == null)
			{
				ConsoleUtils.WriteWarning("Shard Mapping not found");
				return;
			}

			DropShardDatabase(shardMapping);
		}

		private void ConfirmDropAll()
		{
			Console.Write("Are you sure you want to DROP ALL DATABASES? (Y/N or leave blank to cancel) ");
			var response = Console.ReadLine();
			if (string.IsNullOrWhiteSpace(response) || response.ToUpperInvariant().Equals("N"))
				return;

			DropAll();
		}

		private static string ReadShardCustomerKey(string verb)
		{
			Console.Write($"Enter the customer key (e.g. \"purdue\") of the shard you wish to {verb} and press ENTER: (or leave blank to cancel) ");
			var customerKey = Console.ReadLine();
			return string.IsNullOrWhiteSpace(customerKey) ? null : customerKey;
		}

		#endregion Console App

		#region Helpers

		/// <summary>
		/// Gets the shard map, if it exists. If it doesn't exist, writes out the reason and returns null.
		/// </summary>
		private ListShardMap<byte[]> TryGetShardMap()
		{
			if (ShardMapManger == null)
			{
				ConsoleUtils.WriteWarning("Shard Map Manager has not yet been created");
				return null;
			}

			var shardMap = ShardUtils.TryGetListShardMap<byte[]>(ShardMapManger,
				ShardConfiguration.ShardMapName);

			if (shardMap != null)
				return shardMap;

			ConsoleUtils.WriteWarning("Shard Map Manager has been created, but the Shard Map has not been created");

			return null;
		}

		private PointMapping<byte[]> TryGetShardMappingByShardKey(string customerKey)
		{
			var shardMap = TryGetShardMap();
			if (shardMap == null)
				return null;
			shardMap.TryGetMappingForKey(customerKey.ToPointMapping(), out var shardMapping);
			return shardMapping;
		}

		#endregion Helpers
	}
}