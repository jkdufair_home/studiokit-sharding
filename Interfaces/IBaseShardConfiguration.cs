﻿using StudioKit.Caliper.Interfaces;

namespace StudioKit.Sharding.Interfaces
{
	public interface IBaseShardConfiguration : ICaliperConfiguration
	{
		string Name { get; set; }

		bool TrialEnabled { get; set; }

		int? TrialDefaultDayLimit { get; set; }

		string UserSupportEmail { get; set; }

		string AllowedDomains { get; set; }

		byte[] Image { get; set; }

		string DefaultTimeZoneId { get; set; }

		bool ValidateAllowedDomains(string scopedIdentifier);

		bool IsInstructorSandboxEnabled { get; set; }

		int? DemoExpirationDayLimit { get; set; }
	}
}