﻿using System;

namespace StudioKit.Sharding.Interfaces
{
	public interface IBaseLicense
	{
		DateTime StartDate { get; set; }

		DateTime EndDate { get; set; }
	}
}