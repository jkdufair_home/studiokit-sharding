using Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudioKit.Sharding.Utils
{
	public static class ShardUtils
	{
		/// <summary>
		/// Tries to get the ShardMapManager that is stored in the specified database.
		/// </summary>
		public static ShardMapManager TryGetShardMapManager(string shardMapManagerServerName,
			string shardMapManagerDatabaseName)
		{
			var shardMapManagerConnectionString =
				ShardConfiguration.GetConnectionString(
					shardMapManagerServerName,
					shardMapManagerDatabaseName);

			if (!SqlDatabaseUtils.DatabaseExists(shardMapManagerServerName, shardMapManagerDatabaseName))
			{
				// Shard Map Manager database has not yet been created
				return null;
			}

			var shardMapManagerExists = ShardMapManagerFactory.TryGetSqlShardMapManager(
				shardMapManagerConnectionString,
				ShardMapManagerLoadPolicy.Lazy,
				out var shardMapManager);

			return !shardMapManagerExists
				? null // Shard Map Manager database exists, but Shard Map Manager has not been created
				: shardMapManager;
		}

		/// <summary>
		/// Creates a shard map manager in the database specified by the given connection string.
		/// </summary>
		public static ShardMapManager CreateOrGetShardMapManager(string shardMapManagerConnectionString)
		{
			// Get shard map manager database connection string
			// Try to get a reference to the Shard Map Manager in the Shard Map Manager database. If it doesn't already exist, then create it.
			var shardMapManagerExists = ShardMapManagerFactory.TryGetSqlShardMapManager(
				shardMapManagerConnectionString,
				ShardMapManagerLoadPolicy.Lazy,
				out var shardMapManager);

			if (shardMapManagerExists)
			{
				ConsoleUtils.WriteInfo("Shard Map Manager already exists");
			}
			else
			{
				// The Shard Map Manager does not exist, so create it
				shardMapManager = ShardMapManagerFactory.CreateSqlShardMapManager(shardMapManagerConnectionString);
				ConsoleUtils.WriteInfo("Created Shard Map Manager");
			}

			return shardMapManager;
		}

		/// <summary>
		/// Creates a new Range Shard Map with the specified name, or gets the Range Shard Map if it already exists.
		/// </summary>
		public static RangeShardMap<T> CreateOrGetRangeShardMap<T>(ShardMapManager shardMapManager, string shardMapName)
		{
			// Try to get a reference to the Shard Map.
			var shardMapExists = shardMapManager.TryGetRangeShardMap(shardMapName, out RangeShardMap<T> shardMap);

			if (shardMapExists)
			{
				ConsoleUtils.WriteInfo("Shard Map \"{0}\" already exists", shardMap.Name);
			}
			else
			{
				// The Shard Map does not exist, so create it
				shardMap = shardMapManager.CreateRangeShardMap<T>(shardMapName);
				ConsoleUtils.WriteInfo("Created Shard Map \"{0}\"", shardMap.Name);
			}

			return shardMap;
		}

		/// <summary>
		/// Creates a new List Shard Map with the specified name, or gets the List Shard Map if it already exists.
		/// </summary>
		public static ListShardMap<T> CreateOrGetListShardMap<T>(ShardMapManager shardMapManager, string shardMapName)
		{
			// Try to get a reference to the Shard Map.
			var shardMapExists = shardMapManager.TryGetListShardMap(shardMapName, out ListShardMap<T> shardMap);

			if (shardMapExists)
			{
				ConsoleUtils.WriteInfo("Shard Map \"{0}\" already exists", shardMap.Name);
			}
			else
			{
				// The Shard Map does not exist, so create it
				shardMap = shardMapManager.CreateListShardMap<T>(shardMapName);
				ConsoleUtils.WriteInfo("Created Shard Map \"{0}\"", shardMap.Name);
			}

			return shardMap;
		}

		/// <summary>
		/// Gets the shard map, if it exists. If it doesn't exist returns null.
		/// </summary>
		public static ListShardMap<T> TryGetListShardMap<T>(ShardMapManager shardMapManager, string shardMapName)
		{
			var mapExists = shardMapManager.TryGetListShardMap(shardMapName, out ListShardMap<T> shardMap);
			return mapExists ? shardMap : null;
		}

		/// <summary>
		/// Adds Shards to the Shard Map, or returns them if they have already been added.
		/// </summary>
		public static Shard CreateOrGetShard(ShardMap shardMap, ShardLocation shardLocation)
		{
			// Try to get a reference to the Shard
			var shardExists = shardMap.TryGetShard(shardLocation, out var shard);

			if (shardExists)
			{
				ConsoleUtils.WriteInfo("Shard \"{0}\" has already been added to the Shard Map", shardLocation.Database);
			}
			else
			{
				// The Shard Map does not exist, so create it
				shard = shardMap.CreateShard(shardLocation);
				ConsoleUtils.WriteInfo("Added shard \"{0}\" to the Shard Map", shardLocation.Database);
			}

			return shard;
		}

		/// <summary>
		/// Finds an existing empty shard, or returns null if none exist.
		/// </summary>
		public static Shard FindEmptyShard(ListShardMap<byte[]> shardMap, string databaseName)
		{
			// Get all shards in the shard map
			var allShards = shardMap.GetShards();

			// Get all mappings in the shard map
			var allMappings = shardMap.GetMappings();

			// Determine which shards have mappings
			var shardsWithMappings = new HashSet<Shard>(allMappings.Select(m => m.Shard));

			// Get the shard (matched by name) that has no mappings, if it exists
			return allShards.SingleOrDefault(s => s.Location.Database.Equals(databaseName) && !shardsWithMappings.Contains(s));
		}

		/// <summary>
		/// Creates a new shard, or gets an existing empty shard (i.e. a shard that has no mappings).
		/// The reason why an empty shard might exist is that it was created and initialized but we
		/// failed to create a mapping to it.
		/// </summary>
		public static Shard CreateOrGetEmptyShard(ListShardMap<byte[]> shardMap, string databaseName, string serverName,
			string databaseEdition)
		{
			// Get an empty shard if one already exists, otherwise create a new one
			var shard = FindEmptyShard(shardMap, databaseName);
			if (shard != null)
				return shard;

			// No empty shard exists, so create one

			// Only create the database if it doesn't already exist. It might already exist if
			// we tried to create it previously but hit a transient fault.
			if (!SqlDatabaseUtils.DatabaseExists(serverName, databaseName))
			{
				SqlDatabaseUtils.CreateDatabase(serverName, databaseName, databaseEdition);
			}

			// Add it to the shard map
			var shardLocation = new ShardLocation(serverName, databaseName);
			shard = CreateOrGetShard(shardMap, shardLocation);

			return shard;
		}

		/// <summary>
		/// Connects to the ShardMapManager and CustomerShardMap to get a list of customer mappings.
		/// Runs the given Function once per mapping, passing the customer/shard key as the single parameter.
		/// </summary>
		/// <param name="shardMap">The shard map, should already be initialized</param>
		/// <param name="asyncFunc">An asynchronous function delegate which takes a customer/shard key as its single parameter.</param>
		/// <returns></returns>
		public static async Task PerformAsyncFuncOnAllShards(ListShardMap<byte[]> shardMap, Func<string, Task> asyncFunc)
		{
			if (shardMap == null)
				throw new Exception("Shard Map does not exist");

			var mappings = shardMap.GetMappings().ToList();
			if (!mappings.Any())
				return;

			foreach (var customerKey in mappings.Select(m => Encoding.UTF8.GetString(m.Value)))
			{
				await asyncFunc(customerKey);
			}
		}

		/// <summary>
		/// Connects to the ShardMapManager and CustomerShardMap to get a list of customer mappings.
		/// Runs the given Function once per mapping, passing the customer/shard key as the single parameter.
		/// </summary>
		/// <param name="shardMap">The shard map, should already be initialized</param>
		/// <param name="action">An action delegate which takes a customer/shard key as its single parameter.</param>
		/// <returns></returns>
		public static void PerformOnAllShards(ListShardMap<byte[]> shardMap, Action<string> action)
		{
			if (shardMap == null)
				throw new Exception("Shard Map does not exist");

			var mappings = shardMap.GetMappings().ToList();
			if (!mappings.Any())
				return;

			foreach (var customerKey in mappings.Select(m => Encoding.UTF8.GetString(m.Value)))
			{
				action(customerKey);
			}
		}
	}
}