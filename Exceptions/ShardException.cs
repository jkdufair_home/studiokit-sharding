using System;
using System.Runtime.Serialization;

namespace StudioKit.Sharding.Exceptions
{
	public class ShardException : Exception
	{
		public ShardException()
		{
		}

		public ShardException(string message)
			: base(message) { }

		public ShardException(string message, Exception innerException)
			: base(message, innerException) { }

		protected ShardException(SerializationInfo info, StreamingContext context)
			: base(info, context) { }
	}
}